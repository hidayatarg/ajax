﻿$(function () {

    $("#tblDepartmanlar").on("click", ".btnDepartmanSil", function () {

        if (confirm("Silme islemden eminmisiniz")) {
            
            var id = $(this).data("id");
            var btn = $(this);

            $.ajax({
                type: "GET",
                url: "/Departman/Sil/" + id,
                success: function () {
                    btn.parent().parent().remove();

                }

            });
            

        }
        

    });

});

//when u check the button from inspection we have Td and the up side parent is tr so it should update itself
