namespace AjaxReq.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateDepartment : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Departmen(Ad) VALUES('HSE')");
            Sql("INSERT INTO Departmen(Ad) VALUES('Muhendislik')");
            Sql("INSERT INTO Departmen(Ad) VALUES('Service')");
            Sql("INSERT INTO Departmen(Ad) VALUES('IT')");
        }
        
        public override void Down()
        {
        }
    }
}
