﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AjaxReq.Models
{
    public class Personel
    {

        public int Id { get; set; }
        public Departman Departman { get; set; }
        public int DepartmanId { get; set; }

        public string Ad { get; set; }
        public string Soyad { get; set; }
        public int Yas { get; set; }
        public int Maas { get; set; }
        public DateTime DogumTarih { get; set; }
        public bool Cinsiyet { get; set; }
        public bool EvliMi { get; set; }

    }
}