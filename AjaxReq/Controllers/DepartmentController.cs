﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AjaxReq.Models;

namespace AjaxReq.Controllers
{
    public class DepartmentController : Controller
    {
        private ApplicationDbContext _context;

        public DepartmentController()
        {
            _context=new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }


        

        // GET: Department
        [HttpGet]
        public ActionResult Index()
        {
            var departman = _context.Departman.ToList();
            return View(departman);
        }

        
       


        
        [HttpPost]
        public ActionResult Kayedet(Departman departman)
        {
            if (departman.Id == 0)
                _context.Departman.Add(departman);
            else
            {
                var guncellecekDepartman = _context.Departman.Find(departman.Id);
                if (guncellecekDepartman == null)
                    return HttpNotFound();
                guncellecekDepartman.Ad = departman.Ad;
            }

            _context.SaveChanges();

            return RedirectToAction("Index", "Department"); //departman controller icinde
        }


        public ActionResult Guncelle(int id)
        {
            //var departman = _context.Departman.SingleOrDefault(c => c.Id == id);
            var departman = _context.Departman.Find(id);

            if (departman == null)
                return HttpNotFound();

            return View("DepartmentForm", departman);
            
        }

        public ActionResult Sil(int id)
        {
            var silencekDepartman = _context.Departman.Find(id);
            if (silencekDepartman == null)
                return HttpNotFound();
            _context.Departman.Remove(silencekDepartman);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }



    }
}